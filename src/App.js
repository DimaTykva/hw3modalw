
import React, { Component } from "react";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";



class App extends Component {


    state = {
        isOpen: false
    }


    getModal = () => {
        this.setState({isOpen : !this.state.isOpen});
    }



    render() {
        return (
            <div className="holder">
                { this.state.isOpen && <Modal isOpen={this.getModal} text={'hello'}/> }
                <Button backgroundColor={'red'} text={'hello'} />

                { this.state.isOpen && <Modal isOpen={this.getModal} text={'good day'}/> }
                <Button backgroundColor={'green'} text={'good day'} />
            </div>
        );
    }
}


export default App;
