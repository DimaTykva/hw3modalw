import React  from "react";
import {Component}  from "react";
import Modal from "../Modal/Modal";

class Button extends Component {

    state = {
        isOpen: false
    }

    handleSubmit (e) {
        e.preventDefault();
    }

   getModal = () => (
        this.setState({ isOpen : !this.state.isOpen   })
    )


    render (){
        return(
            <div className='btn'>
                { this.state.isOpen && <Modal isOpen={this.getModal}/> }
                <button onClick={ () => this.getModal() }  onSubmit={this.handleSubmit} style={{backgroundColor: this.props.backgroundColor}} >{this.props.text}</button>
            </div>

        )
    }

}


export default Button;
